#!/usr/bin/env python3

import urllib.request
import os, sys, json, fnmatch, subprocess
from time import sleep
from pathlib import Path
import firebase_admin
from firebase_admin import firestore
import concurrent.futures

ROOT_FOLDER = "src"
PROJECT = "gcp-cpe-1-l-secured-k895"
LOCAL_ENDPOINT = "localhost:8071"


class Firebase:
    def __init__(self):
        self.executor = concurrent.futures.ThreadPoolExecutor()
        self.ignore_patterns = []
        if sys.argv[len(sys.argv) - 1] == "--applyignore":
            with open(".fsignore") as file:
                self.ignore_patterns = [line.rstrip() for line in file]
        firebase_admin.initialize_app()
        self.db = firestore.Client(project=PROJECT)

    def ignore_path(self, path):
        for pattern in self.ignore_patterns:
            if fnmatch.fnmatch(path, pattern):
                return True
        return False

    def imports(self):
        print("Importing: ", end="", flush=True)
        # Walks through our local folder, and writes all the files to firestore
        for root, folder_directories, files in os.walk(ROOT_FOLDER, topdown=False):
            if self.ignore_path(root) or root == ROOT_FOLDER:
                continue
            if not len(root.split("/")) % 2:
                for directory in folder_directories:
                    if not os.path.isfile(root + "/" + directory + ".json"):
                        # Create an empty entry for this folder
                        self.db.collection(
                            root.removeprefix(ROOT_FOLDER + "/")
                        ).document(directory).set({})
            for file in files:
                with open(root + "/" + file) as f:
                    file_data = json.load(f)
                    name = os.path.splitext(file)[0]
                    self.db.collection(root.removeprefix(ROOT_FOLDER + "/")).document(
                        name
                    ).set(file_data)
                print(".", end="", flush=True)
        print("Import completed")

    def exports(self, collections):
        # Export all data from our selected collections
        for collection in collections:
            print(f"Exporting: [{collection}]", end="", flush=True)
            self.execute_on_remote_collections(collection)
            # print("Done")
        # print("Export completed")

    def execute_on_remote_collections(self, item):
        # This functions loops through all the files/subcollections in a collection
        # It then will preform an action on those items (export/delete)
        path = ROOT_FOLDER + "/" + item
        if self.ignore_path(path):
            return
        root_ref = self.db.collection(item)
        docs = root_ref.stream()
        # Loop through all the documents in this collection
        for doc in docs:
            doc_data = doc.to_dict()
            doc_id = doc.id
            path_with_doc_id = path + "/" + doc_id
            if self.ignore_path(path_with_doc_id):
                continue
            # export files
            try:
                os.makedirs(path)
            except OSError as _:
                pass
            Path(path_with_doc_id + ".json").write_text(
                json.dumps(doc_data, indent=2, default=str)
            )
            print(".", end="", flush=True)
            # Continue down subcollections in collection
            for collection_ref in doc.reference.collections():
                threads.append(
                    self.executor.submit(
                        self.execute_on_remote_collections,
                        str(collection_ref.parent.path + "/" + collection_ref.id),
                    )
                )


#########################


def emulate():
    os.environ["FIRESTORE_EMULATOR_HOST"] = LOCAL_ENDPOINT

    # Check if emulator is already running
    try:
        urllib.request.urlopen("http://" + LOCAL_ENDPOINT).read()
        print(f"{BColors.BLUE}---- Emulator is already running ----{BColors.ENDC}")
        return
    except:
        # If not, run a new instance
        pro = subprocess.Popen(
            [
                "firebase",
                "emulators:start",
                "--only",
                "firestore",
                "--project",
                PROJECT,
            ],
            # shell=True # Windows compatibility
        )

    def wait_for_emu():
        print(f"{BColors.BLUE}---- Waiting for emulator ----{BColors.ENDC}")
        for _ in range(24):
            try:
                print(urllib.request.urlopen("http://" + LOCAL_ENDPOINT).read())
                return
            except:
                if pro.poll() is not None:
                    break
                sleep(5)
        raise Exception("Could not load emulator")

    def import_local_collections():
        print(f"{BColors.YELLOW}---- Emulator found, writing data ----{BColors.ENDC}")
        Firebase().imports()  # import local data to emulator

    def let_emu_run():
        print(f"{BColors.GREEN}---- Emulator is up. Go code! ----{BColors.ENDC}")
        try:
            pro.wait()
        except:
            pass

    try:
        wait_for_emu()
        import_local_collections()
        let_emu_run()
    except Exception as e:
        print(f"{BColors.RED}Error: {str(e)}\n---- Shutting down ----{BColors.ENDC}")
        pro.terminate()  # Send the signal to the process
    sleep(1)


#########################


class BColors:
    BLUE = "\033[94m"
    GREEN = "\033[92m"
    YELLOW = "\033[93m"
    RED = "\033[91m"
    ENDC = "\033[0m"


threads = []

if __name__ == "__main__":
    if len(sys.argv) >= 2 and sys.argv[1] == "--export":
        collections = []
        if len(sys.argv) >= 3:
            collections = sys.argv[2].split(",")
        try:
            urllib.request.urlopen("http://" + LOCAL_ENDPOINT).read()
            print("Emulator found: Exporting emulator collections.\n")
            os.environ["FIRESTORE_EMULATOR_HOST"] = LOCAL_ENDPOINT
        except:
            print("No Emulator found: Exporting from remote collections.\n")
        Firebase().exports(collections)
        # Wait for all threads
        for thread in concurrent.futures.as_completed(threads):
            result = thread.result()
            if result:
                print(result)
    else:
        emulate()
