#!/usr/bin/env python3

import os, sys, json, fnmatch
import concurrent.futures
import firebase_admin
from firebase_admin import firestore
from google.oauth2.credentials import Credentials

threads = []
ROOT_FOLDER = "src"
AUTH_TOKEN = json.loads(os.environ.get("CLOUDSDK_AUTH_ACCESS_TOKEN"))["accessToken"]
PROJECT = os.environ.get("PROJECT")
IMPORT_DATABASES = json.loads(os.environ.get("IMPORT_DATABASES"))


class Firebase:
    def __init__(self, database):
        self.executor = concurrent.futures.ThreadPoolExecutor()
        self.database = database
        self.ignore_patterns = []
        if sys.argv[len(sys.argv) - 1] == "--applyignore":
            with open(".fsignore") as file:
                self.ignore_patterns = [line.rstrip() for line in file]
        self.db = firestore.Client(
            project=PROJECT,
            database=database,
            credentials=Credentials(AUTH_TOKEN),
        )

    def ignore_path(self, path):
        for pattern in self.ignore_patterns:
            if fnmatch.fnmatch(path, pattern):
                return True
        return False

    def imports(self):
        print(f"--Importing on [{self.database}]")
        # Walks through our local folder, and writes all the files to firestore
        for root, folder_directories, files in os.walk(ROOT_FOLDER, topdown=False):
            if self.ignore_path(root) or root == ROOT_FOLDER:
                continue
            # Check and make sure collections have their subsequent database entries
            if not len(root.split("/")) % 2:

                def check_valid_dir(root, directory):
                    if not os.path.isfile(root + "/" + directory + ".json"):
                        # Create an empty entry for this folder
                        self.db.collection(
                            root.removeprefix(ROOT_FOLDER + "/")
                        ).document(directory).set({})

                for directory in folder_directories:
                    threads.append(
                        self.executor.submit(check_valid_dir, root, directory)
                    )

            # Write all the files that exist to Firestore
            def writer(root, file):
                with open(root + "/" + file) as f:
                    name = os.path.splitext(file)[0]
                    try:
                        file_data = json.load(f)
                        self.db.collection(
                            root.removeprefix(ROOT_FOLDER + "/")
                        ).document(name).set(file_data)
                    except Exception as e:
                        return f"Error: {root}/{name}.json - {e}"
                print(f"Imported: {root}/{name} to [{self.database}]")

            for file in files:
                threads.append(self.executor.submit(writer, root, file))
        print(f"--Import completed for [{self.database}]")

    def deletes(self):
        print(f"--Deleting on [{self.database}]")
        # Delete all documents not in our local static folder
        for delete in os.listdir(ROOT_FOLDER):
            self.execute_on_remote_collections(delete)
        print(f"--Delete completed for [{self.database}]")

    def execute_on_remote_collections(self, item):
        # This functions loops through all the files/subcollections in a collection
        # It then will preform an action on those items (export/delete)
        path = ROOT_FOLDER + "/" + item
        if self.ignore_path(path):
            return
        root_ref = self.db.collection(item)
        docs = root_ref.stream()
        # Loop through all the documents in this collection
        for (
            doc
        ) in (
            docs
        ):  # Unfortunately this can not be turned into a concurrent thread, it ruins the traversal by deleting directories before we can go down them
            doc_id = doc.id
            path_with_doc_id = path + "/" + doc_id
            if self.ignore_path(path_with_doc_id):
                return  # continue
            # delete files not in our static folder
            if not os.path.isfile(path_with_doc_id + ".json") and not os.path.isdir(
                path_with_doc_id
            ):
                root_ref.document(doc_id).delete()
                print(f"Deleted: {path_with_doc_id} from [{self.database}]")
            # Continue down subcollections in collection
            for collection_ref in doc.reference.collections():
                threads.append(
                    self.executor.submit(
                        self.execute_on_remote_collections,
                        str(collection_ref.parent.path + "/" + collection_ref.id),
                    )
                )


#########################


if __name__ == "__main__":
    firebase_admin.initialize_app()  # Initialize app
    print(f"Using project: [{PROJECT}]")
    executor = concurrent.futures.ThreadPoolExecutor()
    for database in IMPORT_DATABASES:
        f = Firebase(database=database)  # update each database
        if len(sys.argv) >= 2 and sys.argv[1] == "--delete":
            # Delete remote files
            threads.append(executor.submit(f.deletes))
        threads.append(executor.submit(f.imports))  # import our new ones
    # Wait for all threads
    done, not_done = concurrent.futures.wait(threads, return_when="ALL_COMPLETED")
    for thread in done.union(not_done):
        result = thread.result()
        # Print any errors
        if result:
            print(f"\033[91m{result}\033[0m")
            exit(1)
