# Firestore Utility Tool

## Prerequisites

- Make sure python 3 is installed.
- Make sure firebase-tools are installed on your machine. If not, you will not be able to run the emulator, and receive an error.

## Setup

In the root folder, run:

`pip3 install --trusted-host pypi.python.org --trusted-host files.pythonhosted.org --trusted-host pypi.org -r requirements.txt`

## fs-emu Usage

This is the emulator only script. It will run the emulator locally and import you local firestore static folder. It ignores all arguments except the ones below.

`--export foo,bar (optional)`

fs-emu provides the ability to export data from the remote collection, or your emulator if it is running. Using this command will not start the emulator. Running this will export "foo" and "bar" into your firestore static folder. Must be comma separated.

`--applyignore (optional)`

Adding this to the end of a usage command will apply the `.fsignore` file, and ignore importing the paths set there. Uses fnmatch patterns: https://docs.python.org/3/library/fnmatch.html

## .fsignore

If `*/foo` is in our ignore file, and the file directory is: `something/foo`, then the folder will be ignored. This includes exports, imports, and deletes.
If you wish to ignore only a certain file, you can do `*/foo/bar`. Notice there is no `.json` ending. This would ignore `bar.json`.

## fs-util Usage

Will import the firestore static folder to the dev project. **THIS DOES NOT DELETE BY DEFAULT.** This will only update files.

`--delete (optional)`

If you wish to delete, you must add `--delete` after the import command. This should be done cautiously (i.e. be mindful of your .fsignore, etc.). When in import & delete mode, the script will remove any files from the remote collection, that are NOT also in your local static folder collection. As an example, if you were to manually add an entry in remote Firestore, but it was not present in your local collection, it would be removed.

`--applyignore (optional)`

Adding this to the end of a usage command will apply the `.fsignore` file, and ignore the paths set there. Uses fnmatch patterns: https://docs.python.org/3/library/fnmatch.html

## fs-util Environment Variables

The pipeline utility takes in a few different variables to operate:

`PROJECT` - The project name

`IMPORT_DATABASES` - A list of strings, of databases, to import to.

## Troubleshooting

1. **Problem**: Export/Import hangs forever OR timeout error.

   **Solution**: Try re-authenticating yourself using `gcloud auth application-default login`

2. **Problem**: "Error: download failed, status 407"

   **Solution**: If you receive an Access denied or credentials could not be verified error on starting the emulator, try using: `firebase login`

3. **Problem**: "A collection must have an odd number of path elements"

   **Solution**: You are trying to add a file in a incorrect way. If you get this error it means you are adding an entry to which is a collection. To fix this, you need to add another directory, and put your file in there.
